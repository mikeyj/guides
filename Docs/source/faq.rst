.. index:: Frequently Asked Questions
.. _contact us: http://primaryt.co.uk/contact-us/
.. _migrate page: https://login.schoolemail.co.uk/migrate/
.. _faq:

Frequently Asked Questions
==========================

What is OWA?
------------

OWA (Outlook Web App) is the name for the web-mail service provided by SchoolEmail.  When you login to access and send emails in a browser (such as Google Chrome, Mozilla Firefox, Internet Explorer) you are using OWA.

What is a Portal Administrator and a SchoolEmail Moderator?
-----------------------------------------------------------

A Portal Administrator is the name given to people who have access to manage your organisations email accounts. A SchoolEmail Moderator is the name given to the person who is chosen to receive emails caught with our pupil protection filters.

Can we get our own custom theme?
--------------------------------

Yes! We can create a custom theme for your school or organisation which consists of a personalised login page and OWA theme.  Our in-house web designers will work with you to create a design to match your existing branding and school website.
For more information please `contact us`_.

Can we increase the number of accounts we have part-way through a subscription?
-------------------------------------------------------------------------------

Yes! Please `contact us`_ with your request, stating the number of additional accounts you would like.  A member of our team will contact you, usually within 24 hours.

Can we transfer our emails from another email service to SchoolEmail?
---------------------------------------------------------------------

Yes! We have a migration tool that will transfer emails into your SchoolEmail account from most email services.
Visit our `migrate page`_ to start transferring your emails into your SchoolEmail account.

What is a Domain / Domain Name / DNS?
-------------------------------------

A Domain is the unique internet address that appears after the @ sign in email addresses and after the www of a website address (www.schoolemail.co.uk is a domain).
It typically takes the form of your organisation's name and a standard Internet suffix, such as *stjames.bradford.sch.uk*.
You can transfer an existing domain name or purchase a new one.  We also provide a free domain of *jnrmail.com*.

Can you host our domain?
------------------------

Yes!  We can host both new and existing domains.  Existing domains will need to be transferred to us from your current domain name holder which can take a few days.
For pricing and information, please `contact us`_.

What is a group?
----------------

A group (also known as a distribution list) is a list of email addresses that can be emailed together.  For example, we automatically create a 'Teachers' group. You would simply email teachers@yourschooldomain.sch.uk and it would send the email to all teachers in school.

Are mailboxes backed up?
------------------------

We backup all mailboxes for disaster recovery, which ensures we never lose any emails for any of our users.

Can we backup individual mailboxes?
-----------------------------------

Yes!  We offer an add-on service to backup individual mailboxes on a daily basis, allowing you to recover a mailbox to a specific point in time.
Please `contact us`_ for more information.

Can we access a mailbox without knowing the password?
-----------------------------------------------------

Yes!  You have full control to grant permission for a user to have full access to another user's account.  This is controlled by your portal administrator.

The SchoolEmail Portal says our subscription is due soon, what happens when it runs out?
----------------------------------------------------------------------------------------

If your subscription runs out before you can renew it, your email accounts will still continue to work and the portal will still be available.
However, you will be unable to create any new accounts until a renewal is confirmed.

How secure is our user data and where is it held?
-------------------------------------------------

We take data protection and security very seriously.  All data is held on servers in the UK and under no circumstances will we use or share that data for any purpose.
The information we hold is only accessible to a limited number of senior employees, all of whom have enhanced CRB checks and are required to sign and adhere to a strict data protection code of practice.
