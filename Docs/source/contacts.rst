.. index:: SchoolEmail Portal Administrator Guides
.. _SchoolEmail Portal: https://portal.schoolemail.co.uk
.. _SchoolEmail Login Page: https://login.schoolemail.co.uk
.. _contacts:

Contacts
########

A contact allows you to make an internal email address for an external email address. Useful when needing to forward emails from a SchoolEmail account to an external account. This also means you could add external users in to your organisations groups.

Create a contact
----------------

.. Note:: When a contact is created, please be aware this is a global contact that can be seen by everyone in the organisation.

To create a contact, first login to our `SchoolEmail Portal`_.

Login with your username and password. (Administrators only)
Then using the navbar at the top go to Contacts > Create Contact.

.. image:: ../source/images/contact_create1.*

On this page you will need to add the two required fields: display name and email address.
Add the details of the contact you would like to create by clicking the blue line to begin adding the information, then click the tick to confirm. 

.. Note:: The email address has to be a valid email address from a non SchoolEmail account.

Once the fields have been filled in, click the create new contact button.

.. image:: ../source/images/contact_create2.*

A blue pop-up box will appear to confirm the contact has been created successfully. Once the new contact has been created, it will immediately appear in your address book and in OWA. Outlook users may have to wait up to 24 hours to see the new contact.
This contact can now also be added to SchoolEmail groups and can be used for forwarding. To forward emails to this contact, see the Forwarding Guide.

.. image:: ../source/images/contact_create3.*

--------

Manage a contact
----------------

To edit an existing contact's details, first login to the `SchoolEmail Portal`_.

Login with your username and password. (Administrators only)
Then using the navbar at the top go to Contacts > Manage Contacts.

.. image:: ../source/images/contact_manage1.*

Here you can see a list of all your contacts. To edit a contact, click the blue pencil icon to the right of the user's name.

.. image:: ../source/images/contact_manage2.*

You can now change the user's details: click either display name or email address, add the required changes and click the blue tick to confirm.

.. Note:: The email address has to be a valid email address from a non SchoolEmail account.

.. image:: ../source/images/contact_manage3.*
