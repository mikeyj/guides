.. index:: Outlook User Guides
.. _outlook:

**************************
SchoolEmail Outlook Guides
**************************

The following guides contain information on the general use and operation of Outlook versions 2010, 2013 and 2016.

For the web based email application please see
:doc:`owa` for more information.

Connect to an account
---------------------

.. Note:: Please make sure your Outlook Application is fully updated before attempting to connect, updates are provided in Windows Updates.

Open your Outlook application, if this is the first time connecting with Outlook you will see a welcome screen to begin with, press next and when asked if you would like to connect to an exchange account, click yes and next.

.. image:: ../source/images/outlook_connect.*

Fill in the details required "Name, Password and Email address".  If you are unsure of these details, ask your organisations email administrator.

.. image:: ../source/images/outlook_connect1.*

Your account should now connect successfully, click finish and let your Outlook collect your emails if any exist.

.. image:: ../source/images/outlook_connect2.*

-------------


Share a calendar
----------------

Open the Outlook account who's calendar is to be shared. Once open, click the calendar button at the bottom of the application and you will see the options on the toolbar change.

Click the option to 'Share Calendar'.

.. image:: ../source/images/outlook_cal1.*

You will find a window has opened, this is where we set the details of the calendar to be shared and who we are going to share it with.

First click the 'details' box to select from the following options:

- Availability Only - Calendar entries will only show as Free, Busy, Tentaive, Working Elsewhere or Out of Office.
- Limited Details - Calendar entries show as above but this option also shows the subject of the entry.
- Full Details - Shows the full details of all calendar entries. 

Once you have selected what details to show, you can now select the 'To' field and add the address of the user or users who requires access to this calendar.
When all users have been added click send. 

.. image:: ../source/images/outlook_cal2.*

After you click send, Outlook will ask you to confirm that you would like to share the calendar. Click yes to confirm and Outlook will send the calendar sharing invite to the selected users.

The users who were added will receive a calendar share invite from you, see the steps below on the how to accept a calendar invite. 

.. image:: ../source/images/outlook_cal3.*

-------------


Accept a calendar share
-----------------------

Login to the account that has received a calendar invite.
You will see a message in your inbox named 'Sharing invitation'. Open this message and you will see the details of the calendar which is being shared.

To accept this invite click the 'Add This Calendar' button. 

.. image:: ../source/images/outlook_cal4.*

You will receive a confirmation that the calender has been added to your list of calendars.

.. image:: ../source/images/outlook_cal5.*

If you go to your calendar page as normal, you will now see the additional calendar in your list.
This calendar has been successfully shared.

.. image:: ../source/images/outlook_cal6.*

-------------


Get email headers
-----------------

Getting the headers of an email is very useful. The headers contain information about the email being sent such as where it came from, what it's return address is and more. 
We may request the headers for any reported emails.

To get the headers of an email, open Outlook as normal. 
Once open, find the email you need to get the headers from and double click the email to open it in a new window. 

The next step is to click the 'File' tab in the top left hand corner of this window.

.. image:: ../source/images/outlook_header1.*

This is the main options page for Outlook, near the bottom middle of the page you will see a button described as 'Properties'.
Click the properties button and you will be taken to another window.

.. image:: ../source/images/outlook_header2.*

At the bottom of this window is a box named 'internet headers'. This is the information we require.
So with your mouse, right click in the box and choose the 'select all' option. You will notice the text within that box has been highlighted, now right click on the text again and this time click the 'copy' option.

The headers are now saved to your clipboard and the next step is to send them onto the relevant person.

.. image:: ../source/images/outlook_header3.*

Now you have copied the headers on to your clipboard, close the window and you will be back to the email window.
Click the forward email button and then in the body of the email right click and select the 'paste' option.

Those headers will now be copied into the email. Make sure to add the address of the person requesting the headers in the 'To' field and click send.
The email with the headers will now be on its way. 

.. image:: ../source/images/outlook_header4.*

-------------


Recover a deleted item
----------------------

All items that are initally deleted get moved into the 'deleted items' folder, here the messages will stay until they are deleted a second time.  If an email is deleted a second time, you will have 30 days to retrieve the email from the mailboxs retention area.

To retrieve an email, right-click the 'deleted items' folder and select 'recover deleted items..'.

.. image:: ../source/images/outlook_deleted1.*

A new window will open listing all the items that can be recovered.  Highlight the item/s to be recovered and select the option to "restore selected items" and click 'ok'.

The selected items will now be moved back into the folders they were deleted from orginally.

.. image:: ../source/images/outlook_deleted1.*

-------------


Block a senders address
-----------------------

Open the Outlook application and find the email from the sender which you wish to block and right click on the email.
In the menu that has appeared, hover the mouse pointer over the 'Junk' option and another few options will appear.

Click the option named 'Block Sender' and the email will be moved to your 'Junk Email' folder. Outlook will now put any email sent from that sender directly in to your Junk email folder instead of your inbox.
Blocking a sender is as easy as that. 

.. image:: ../source/images/outlook_block1.*

-------------


Whitelist a senders address
---------------------------

Open the Outlook application and find the email from the sender which you wish to whitelist and right click on the email.
In the menu that has appeared, hover the mouse pointer over the 'Junk' option and another few options will appear.

Click the option named 'Never Block Sender' and the email will be moved to your 'Inbox' folder if not already in it.

.. image:: ../source/images/outlook_white1.*

A pop up box will appear stating the address has been added to your safe senders list.  Outlook will now ensure any email from that sender will get delivered directly in to your Inbox.
Whitelisting a sender is as easy as that. 

.. image:: ../source/images/outlook_white2.*

-------------


Create your own email signature
-------------------------------

Open your Outlook application and click the 'File' tab in the top left hand corner.

.. image:: ../source/images/outlooksig1.*

Down the left hand side click the 'Options' button.

.. image:: ../source/images/outlooksig2.*

In this window, click the 'Mail' button and we can now click the 'Signatures...' button. 

.. image:: ../source/images/outlooksig3.*

First thing we can do is create our signature name. Click 'New' and give your signature a name which just allows you to have multiple signatures. 

.. image:: ../source/images/outlooksig4.*

Now you can add the details of your signature using a variety of Fonts, Sizes and Colours. When you're happy, click the save button to save the signature.

To select the default signature on new emails and replies/forwards, click the dropdown boxes next to the corresponding options and select your signature name.
Click 'ok' and the settings will be applied. 

.. image:: ../source/images/outlooksig5.*


Clear your auto-complete cache
------------------------------

Open your Outlook application and click the 'File' tab in the top left hand corner.

.. image:: ../source/images/outlooksig1.*

Down the left hand side click the 'Options' button.

.. image:: ../source/images/outlooksig2.*

In this window, click the 'mail' button and scroll down untill you see the 'Empty Auto-Complete List' button and click this.

.. image:: ../source/images/outlook_auto1.png

A window will appear asking you to confirm your decision to clear the auto-complete cache. Click yes and all auto-complete entries will be removed.

.. image:: ../source/images/outlook_auto2.png
