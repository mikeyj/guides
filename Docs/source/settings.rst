.. index:: SchoolEmail Portal Administrator Guides
.. _SchoolEmail Portal: https://portal.schoolemail.co.uk
.. _SchoolEmail Login Page: https://login.schoolemail.co.uk
.. _contact us: https://primaryt.co.uk/contact-us/
.. _settings:

Settings
########

Enable pupil protection
-----------------------

Pupil protection is a custom filter for your SchoolEmail accounts. The filter checks every inbound and outbound email against a list of vulgar words and sexual predator phrases.

When you enable this filter, you have the option to either copy the message to a 'moderator' contact (both the recipient and the moderator will receive the email), or have it redirect completely (so only the moderator receives the flagged email).

The SchoolEmail Moderator can be any email account or contact within SchoolEmail.

To enable pupil protection, first login to the `SchoolEmail Portal`_.  Login with your username and password. (Administrators only)
Then using the navbar at the top, click System.

.. image:: ../source/images/setting1.*

On this page you will see the Pupil Protection setting. The Status states whether the filter is enabled or disabled. Click the switch to change between disabled and enabled.
The Moderator is the email address of a person who will receive any emails that get flagged by our pupil protection filter.

The Action is whether a flagged email gets delivered and copied to the moderator or gets re-directed to the moderator only. 

.. image:: ../source/images/setting_pp1.*

Once you have changed your settings, click the blue Update button and a confirmation will appear. Click confirm and allow up to 30 minutes for the settings to apply.

.. image:: ../source/images/setting_pp2.*

-------------

Add a custom disclaimer
-----------------------

.. Note:: This guide is for adding a organisation wide disclaimer to all emails sent externally.  For individual signatures, see the apporpriate OWA/Outlook guides.

A custom disclaimer is a notice or warning that is added to all outgoing email. It forms a distinct section which is separate from the main email message, and can be used to explain your organisation's policy on confidentiality, copyright, contract information, defamation, discrimination, harassment, and privileges.

To create a custom disclaimer, first login to the `SchoolEmail Portal`_.  Login with your username and password. (Administrators only)
Then using the navbar at the top, click System.

.. image:: ../source/images/setting1.*

You will be able to see the custom disclaimer box. Enter the text of the disclaimer into this box and click 'Update' to save the changes.

.. Note:: Only text can be displayed for the disclaimer, it cannot contain images.

.. image:: ../source/images/setting_disclaimer.*

Click confirm and you will find your disclaimer has now been applied. It will be attached to all emails sent from within your organisation, above SchoolEmail's own disclaimer.

.. image:: ../source/images/setting_disclaimer1.*

-------------

Address book segregation
------------------------

Address book segregation is used when you would like to change who sees who in the address book for your users. When selecting 'staff' you can allow them to see just other staff users or all users. When selecting 'pupils' you can allow them to see other pupils, staff or both.

To enabled address book segregation, first login to the `SchoolEmail Portal`_.
Login with your username and password. (Administrators only)

Then using the navbar at the top, click System.

.. image:: ../source/images/setting1.*

Once on the system page, find the panel named Address Book Segregation. Select which type of user you would like to change (Staff or pupil) and then use the dropdown to select which users can be seen in the address book.

Click the blue tick icon to confirm your selection. Once the desired settings have been chosen click the blue update button.

.. image:: ../source/images/setting_seg1.*

You will see a confirmation window appear. Click confirm and the changes will be applied.
Please allow up to 30 minutes for your users address books to update.

.. Note:: Address books in Outlook can take up to 8 hours to update as they use an offline copy of the address book.

.. image:: ../source/images/setting_seg2.*

-------------

Pupil mail flow restrictions
----------------------------

Mail flow settings are also known as Pupil Email Restrictions and allow you to set whether your pupils can send and/or receive emails from external sources i.e. whether they can email users outside of SchoolEmail. 

To enable pupil mail flow restrictions, first login to the `SchoolEmail Portal`_.
Login with your username and password. (Administrators only)
Then using the navbar at the top, click System.

.. image:: ../source/images/setting1.*

On the systems page, find the pupil email restrictions panel. The direction field here can be clicked and you will be presented with the following options:

- Inbound Email - Pupils cannot receive email from users outside of SchoolEmail.
- Outbound Email - Pupils cannot send emails to users outside of SchoolEmail.
- Both - pupils cannot send or receive emails from users outside of SchoolEmail.

Once the required direction has been selected click the blue tick icon to confirm your selection. Now click the blue update button to apply the changes.

.. image:: ../source/images/setting_flow1.*

As this is a major change to the mail flow of your users, you will be asked to confirm your decision. Click confirm and your changes will be applied.

.. image:: ../source/images/setting_flow2.*

