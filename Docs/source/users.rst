.. index:: SchoolEmail Portal Administrator Guides
.. _SchoolEmail Portal: https://portal.schoolemail.co.uk
.. _SchoolEmail Login Page: https://login.schoolemail.co.uk
.. _users:

Users
####

The following guides are for use with the `SchoolEmail Portal`_.  The guides contain information on how to create/delete and modify user email accounts.

Create a user
-------------

To create a user, first login to our online `SchoolEmail Portal`_.
Login with your username and password. (Administrators only)

Then using the navbar at the top go to Users > Create New User.

.. image:: ../source/images/owa_user1.*

Fill in the details of the user you would like to create, as the example below shows.
Once you have added all the information needed click 'Create New User'.

.. image:: ../source/images/owa_user2.*

Providing your organisation has enough spare premium / standard accounts, your new user will be created.
You can now login to the created account via our `SchoolEmail Login Page`_.

.. image:: ../source/images/owa_user3.*

------------

Manage a user
-------------

You can easily edit any of your users' details using our online management portal.
To edit a user, first login to the `SchoolEmail Portal`_.

Login with your username and password. (Administrators only)
Then using the navbar at the top go to Users > Manage Users.

.. image:: ../source/images/owa_user4.*

On this page you will see a list of all your users. To narrow the list down, you can use the search filter.

The search filter lets you select from a number of options, for example if you would like to view only pupil accounts, change the user type to pupil and click update, this will change the user list to only show pupil accounts.
When you have found the desired user, click the blue pencil icon to the right of the user. 

.. image:: ../source/images/owa_user5.*

You will now be taken to this edit user page. Here you can click any of the values such as first name, last name, password etc.
When you click the desired value, a box will appear and will allow you to edit the contents, once edited click the blue tick box to save your changes.

You may notice some values such as account status and email address policy are are button switches - these are features that can be turned on or off just by clicking the button. 

.. image:: ../source/images/owa_user6.*

------------

Delete a user
-------------

.. Note:: SchoolEmail has a two-stage delete, this is used to reduce accidental deletions when using the bulk delete option in our `SchoolEmail Portal`_.

When you first delete a user, they will be disabled and scheduled for deletion after 30 days. This retention period means the user will be kept on our system for 30 days before being permanently deleted.

If you require that user to be removed immediately, you may delete an already deleted user a second time to immediately and permanently delete them.
To delete a user, first login to the `SchoolEmail Portal`_.

Login with your username and password. (Administrators only)
Then using the navbar at the top go to Users > Manage Users.

.. image:: ../source/images/owa_user4.*

On this page you will see a list of all your users. To narrow the list down you can use the search filter. 
When you have found the desired user, click the blue pencil icon to the right of the user. 

.. image:: ../source/images/owa_user5.*

This is the edit user page, scroll to the bottom of the page and click the delete user button.
You will now have added that user into a retention period of 30 days. Any deleted users will be held on our system for 30 days before being permanently deleted. 

.. image:: ../source/images/owa_user7.*

When a user is deleted for the first time, they will still appear in your address list and groups.
If you require these users to be removed immediately, delete the user a second time by repeating the process. On the second delete you will receive a confirmation.

Please be aware, when you delete a user for the second time, the user will be completely removed from all address lists and groups as well as having their mailbox and the emails it contains deleted permanently.

.. image:: ../source/images/owa_user8.*

------------

Email forwarding
----------------

Here we will go through the required steps on how to setup forwarding from a SchoolEmail account to a non-SchoolEmail account. You will first need to create a contact for the external email address. See our 
:doc:`contacts` guides for more information.

Once you have a contact created we can begin to setup the forwarding. First, login to the `SchoolEmail Portal`_.
Login with your username and password. (Administrators only)

Then using the navbar at the top go to Users > Manage Users.

.. image:: ../source/images/owa_user4.*

On this page you will see a list of all your users. To narrow the list down you can use the search filter. 
When you have the user that requires their emails to be forwarded, click the blue pencil icon to the right of the user. 

.. image:: ../source/images/owa_user5.*

You will now be taken to the edit user page. Find the field named 'Forwarder' and begin typing the name of the user (a SchoolEmail user account or an external contact may be selected here).

After you begin typing, a dropdown box will appear with a list of users based on your search.
Click the desired user from the dropdown, then click the blue tick icon and you will be asked to confirm your selection.

Now that forwarding has been applied, a new option becomes available below. 

.. image:: ../source/images/owa_user9.*

Below the forwarder field there is now another field called 'Deliver and redirect'.

This is disabled by default. If disabled, emails will be forwarded without saving a copy. If enabled, emails will be forwarded and a copy will be saved in the mailbox that is being edited.
Choose the delivery method you require and the forwarding will now be applied. 

.. image:: ../source/images/owa_user10.*

------------

Calendar/Mailbox permissions
----------------------------

User permissions are broken down into two types, calendar permissions and mailbox permissions. Calender permissions allows users to view other users calendars with just read access or both read and write access.
Mailbox permissions allow users to access other users email accounts. 

.. Note:: Full access is required to view another users account via OWA.

To manage your user permissions, you can login to the `SchoolEmail Portal`_.
Login with your username and password. (Administrators Only)

Using the navbar at the top go to the users dropdown and click 'permissions'.

.. image:: ../source/images/owa_user11.*

In the 'User' field start typing the name of the user who is needing access to a mailbox or calendar. As you type you will be presented with a list of users, select the desired user and click the tick to confirm.

Now in the 'Target User' field start typing the name of the user who's mailbox or calendar the first user is needing access to. Again as you type you will be presented with a list of users, select the desired user and click the tick to confirm.

The 'Permission Type' field is where you select the type of permission you require.

- Calendar Read Only - Allows user to view but not edit target users calendar.
- Calendar Full Access - Allows user to view and edit target users calendar.
- Mailbox Read Only - Allows user to add the target users inbox to their OWA/Outlook account, but will be unable to send messages as the target user.
- Mailbox Full Access - Allows user to add the target users inbox to their own OWA/Outlook accounts and also allows the user to send messages on behalf of the target user. 

Once the required permissions are selected click the blue 'add' button.

.. image:: ../source/images/owa_user12.*

You will be asked to confirm, once confirmed please allow up to 30 mins to apply.

.. image:: ../source/images/owa_user13.*

------------

Export a list of user accounts
------------------------------

You can now select and download a list of your users' details using the `SchoolEmail Portal`_.
To export a users list, first login to the `SchoolEmail Portal`_.

Login with your username and password. (Administrators only)
Then using the navbar at the top go to Users > Manage Users.

.. image:: ../source/images/user_export.*

On this page you will see a search filter followed by a list of your users. If you want to narrow down the users before exporting, use the search filter by changing one or multiple fields to suit the list you require and click update to produce a new list with your chosen filter/s.

When you have the desired user list you want to export, click the blue export list button at the bottom of the search filter.

.. Note:: There may be user accounts waiting to be deleted that will not be counted in the list, change the Deleted filter to yes and click update to check on accounts in retention.

.. image:: ../source/images/user_export1.*

After clicking export list, you wll get a request to download a .csv file which you can choose to save or open.
Your computer may automatically choose the best application to open this file, however if you get prompted to choose an application, select Excel or Notepad.

.. image:: ../source/images/user_export2.*

