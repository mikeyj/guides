.. _SchoolEmail login: https://login.schoolemail.co.uk
.. _SchoolEmail Portal: https://portal.schoolemail.co.uk
.. _Read The Docs: https://readthedocs.io
.. _Outlook: outlook.html
.. _OWA: owa.html
.. _contact us: http://primaryt.co.uk/contact-us/

The SchoolEmail Wiki
====================

Welcome to the SchoolEmail Wiki where you will find a comprehensive list of information and guides on using `Outlook`_, `OWA`_ and the `SchoolEmail Portal`_.

SchoolEmail is an email service originally built with schools in mind, its aim is to provide a secure, reliable and affordable service to educational organisations accross the world.
If you would like more information on SchoolEmail please `contact us`_.

This Wiki was created using `Read The Docs`_.

.. toctree::
   :hidden:

   Portal Administrator Guides <portal>
   OWA User Guides <owa>
   Outlook User Guides <outlook>
   Misc Info <misc>
   Frequently Asked Questions <faq>

