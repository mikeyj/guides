.. index:: SchoolEmail Portal Administrator Guides
.. _SchoolEmail Portal: https://portal.schoolemail.co.uk
.. _SchoolEmail Login Page: https://login.schoolemail.co.uk
.. _owa: owa.html


***************************************
SchoolEmail Portal Administrator Guides
***************************************

The `SchoolEmail Portal`_ is a website used to manage an organizations email accounts.

The wesbite that provides a secure and easy to use interface for creating new email accounts, changing passwords, global settings and more.  

This portal also has bulk options available, this allows you to make changes to multiple accounts at the same time, saving precious time managing large organisations.


.. toctree::
   :hidden:

   Users <users>
   Groups <groups>
   Contacts <contacts>
   Spam <spam>
   Settings <settings>
   