.. index:: Online Web App User Guide
.. _owa:

.. _adding user permissions: users.html#calendar-mailbox-permissions
.. _SchoolEmail login: https://login.schoolemail.co.uk

==========================
OWA User Guides
==========================

The following guides contain information on the general use and operation of SchoolEmail's Online Web App.

If you are using Outlook, please see our 
:doc:`outlook` for more information.

Change password
===============

To change your password, first make sure you are logged into your webmail account using our `SchoolEmail login`_ page.

Once logged in, click the cog in the top right hand corner of the screen and a drop down list will appear.

.. image:: ../source/images/view_owa3.*

Click the 'change password' option on this dropdown and you will be taken to the change password page.

.. image:: ../source/images/view_owa6.*

Now you can add your old password and then type in your new password twice and click save, your password has now been changed.

-------------


Block a senders address
=======================

To block a senders address, first make sure you are logged into your webmail account using the `SchoolEmail login`_ page.

Now find the email from the sender which you wish to block and click on it to open the email.  
Once it is open, click the '...' button located at the top right of the window.  This will show a list of options, click 'Mark as junk'.

The email will now have been moved into your junk folder and the senders address has been added to the list of blocked senders which will mean OWA will automatically put all emails from this sender directly into your junk folder.

.. image:: ../source/images/owa_block1.*

Blocking a sender is as easy as that. 

-------------


Whitelist a senders address
===========================

To whitelist a senders address, first make sure you are logged into your webmail account using the `SchoolEmail login`_ page.

Now find the email from the sender which you wish to whitelist and click on it to open the email.  
Once it is open, click the '...' button located at the top right of the window.  This will show a list of options, click 'Mark as not junk'.

The email will now have been moved into your Inbox folder and the senders address has been added to the list of allowed senders which will mean OWA will automatically put all emails from this sender directly into your Inbox.

.. image:: ../source/images/owa_white1.*

Whitelisting a sender is as easy as that. 

-------------


Get email headers
=================

Getting the headers of an email is very useful. The headers contain information about the email being sent such as where it came from, what it's return address is and more. 
We may request the headers for any reported emails.

To get the headers of an email, login to your account using the `SchoolEmail login`_ page.

Now you will have a list of your emails in front of you. Find the email you wish to get the headers from and double click it, the email will open in a new window.

.. image:: ../source/images/owa_headers1.*

Along the top right hand side of the window there is a button with three dots, click this.
This will open a set of options, click "View message details".

.. image:: ../source/images/owa_headers2.*

The message details box will have opened. This is the information we require.

Using your mouse, right click in the box and choose the 'select all' option. You will notice the text within that box has been highlighted, now right click on the text again and this time click the 'copy' option.

The headers are now saved to your clipboard and the next step is to send them onto the relevant person. 

.. image:: ../source/images/owa_headers3.*

Now you have copied the headers on to your clipboard, close the window and you will be back to the email window.
Click the forward email button and then in the body of the email right click and select the 'paste' option.

Those headers will now be copied into the email. Make sure to add the address of the person requesting the headers in the 'To' field and click send.
The email with the headers will now be on its way. 

.. image:: ../source/images/owa_headers4.*

-------------


View another users email account
================================

You cannot login directly to another user's account without knowing the password, however you can view it through another account using OWA. This requires permissions to be added for one account to access another account and as such can only be applied by a Portal Administrator.

To do this you will need to give a user (the account you will use to view another account) full mailbox access to the target user (the account that needs to be viewed).

Please see our guide on `adding user permissions`_. 

Once the permissions have been applied, log in to the account using our `SchoolEmail login`_. page and click the name in the top right hand corner of the browser window, a dropdown will appear.  Click the "Open another mailbox" option. 

.. image:: ../source/images/view_owa1.*

Begin typing the name of the target user in the box that appears and a list of users matching the name will appear, click on the desired user and then click open.

.. image:: ../source/images/view_owa2.*

You will now be taken in to the selected user's mailbox. You will not be able to send any emails or view the calendar using this method but you will be able to view any emails that the mailbox may contain.

-------------


Create your own email signature
===============================

Login to your account using our `SchoolEmail login`_. page and click the cog in the top right hand corner of the screen and a drop down list will appear. From this list click the "options" button

.. image:: ../source/images/view_owa3.*

After the page has opened you will see a number of options down the left hand side, click the "settings" tab.

.. image:: ../source/images/view_owa4.*

Now we can see the email signature box, add the signature you would like to see on the bottom of your emails.
You can change the font, text size, colour as well as adding links to your websites and more.

Once you are happy with your signature, tick the box underneath to automatically include the signature on every email you send.
Now just make sure to click the save button at the bottom of the page before closing the window and your signature settings will be saved.

.. image:: ../source/images/view_owa5.*

-------------


Share a calendar
================

To begin, login to the account who's calendar is to be shared via the `SchoolEmail login`_ page.

Once logged-in, click the calendar button along the top right of the webpage and right click on the calendar to be shared.
You will see the additional calendar options and now you can click 'Share Calendar'.

Now just click 'share calendar'.

.. image:: ../source/images/owa_cal1.*

You will find a new window has opened, this is where we add the address of the people you would like to share the calendar with.

Begin typing the address and a drop down will appear of the users in your address list, select the users you would like to share the calendar with.

.. image:: ../source/images/owa_cal2.*

Another drop down will have appeared with different options, select one of the following options that suits your needs.

- Availability Only - Calendar entries will only show as 'Free' or 'Busy'.
- Limited Details - As above but this option also shows the subject and location fields.
- Full Details - Shows all information contained in the calendar entries.
- Editor - Shows all information contained in the calendar entries and allows editing .
- Delegate - Shows all information contained in the calendar entries, allows editing and allows further sharing requests to be sent/accepted. 

Once you have selected which permissions to give, you can now click 'send' and the sharing request will be sent.

.. image:: ../source/images/owa_cal3.*

The users who were added will receive a calendar share invite from you, see the steps below on the how to accept a calendar invite.

-------------


Accept a calendar share
=======================

Login to the account that has received a calendar invite.
You will see the sharing request as an email in your inbox. Open this message and you will see the details of the calendar which is being shared.

To accept this invite click the 'Add Calendar' button. 

.. image:: ../source/images/owa_cal4.*

Now that the Calendar has been shared.  Go to the calendar page and you will see an additional calendar has been added.
Click the tick-box to bring up that calendar.

When multiple calendars are ticked, the events of both calendars will be merged and have a different colour to differentiate them. You can split the calendar down by choosing the "day" view and then clicking the "split" option. 

.. image:: ../source/images/owa_cal5.*

