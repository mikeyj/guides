.. index:: Misc
.. _Get headers via OWA: owa.html#get-email-headers
.. _Get headers via Outlook: outlook.html#get-email-headers
.. _contact form: http://primaryt.co.uk/contact-us/
.. _contact us: http://primaryt.co.uk/contact-us/
.. _www.staysafeonline.org: https://www.staysafeonline.org
.. _Block an address via OWA: owa.html#Block-a-senders-address
.. _Block an address via Outlook: outlook.html#Block-a-senders-address
.. _Portal external whitelist guide: spam.html#external-spam-whitelist
.. _misc:

Misc
====

Rejected, failed or bounced emails.
---------------------------

Emails can fail to be delivered for a number of reasons, some of the most common reasons an email gets rejected/bounced is:

- Email was sent to a non-existent address - this is common when an email address has been typed incorrectly or no longer exists.
- The recipient's mailbox is full - when an account has reached it's maximum storage capacity.
- The email gets rejected as possible spam - there are many email services and each has its own methods of checking and flagging spam, as such sometimes legitimate emails get caught as spam and are rejected. (common causes are no subject and only using CAPITALS in your emails)
- Compromised accounts leading to blacklisting of email services - a compromised account on a legitimate email service can get that email service blacklisted which means they get added to a database of known spammers. 

When this happens other users of that email service will find their emails will be rejected until the blacklisting has been lifted. 

.. image:: ../source/images/misc_reject.*

Most of the time the cause can be seen in the failed email that is returned. In our example it has returned with "554 5.4.4 SMTPSEND.DNS.NonExistentDomain; nonexistent domain" as that domain does not exist.

Other possible failure messages could be:

- Recipient address rejected: User unknown in relay recipient table - the address you are sending to does not exist. Verify the address is correct, there are a number of email address verifiers online.
- Delivery expired (message too old) '4.7 - Delivery expired (message too old)' (delivery attempts: 0) - often received after a delay usually meaning the recipients mailbox is full. Contact the recipient and/or retry.
- Service unavailable; Unverified Client host [smtpout1.vmhdgvko.co.uk] blocked using dbl.spamhaus.org; - the sender has been blacklisted. Only the service provider of the sender can resolve this. 

It should also be noted, some email services may not send any details when rejecting an email. Contacting the service provider is normally the best course of action when this is the case.

If a sender is being rejected for reasons other than those described, please ask the sender to send the failed email response to us, using our `contact form`_.

- `Get headers via OWA`_
- `Get headers via Outlook`_

-----------

What is Spam
------------

The definition of spam when applied to e-mail is Unsolicited Bulk E-mail, e-mail that has been sent in bulk to recipients who have not given permission for the message to be sent to them. Spam is also known as junk e-mail.

Spamming is flooding the Internet with many copies of the same message to multiple addresses. Spam messages are almost always commercial or fraudulent messages, and are quite often sent with false return address information. The usual reason spam is sent is to attempt to infect unsuspecting users with viruses that send more spam, or to try to steal information from the users. You can find out more at `www.staysafeonline.org`_.

A rapidly growing form of spam is what is known as a Phishing scam. These are messages sent in bulk to a random list of people, claiming to be from a company they may trust, such as a bank or e-bay. These Phishing e-mails will look absolutely genuine to even the most experience Internet user, but will in fact be linking through to a fraudsters website with the aim of stealing personal information from you. Spammers quite often fake the 'From' address of a message, that way the message will not be returned to them in the case of any problems.

Another type of spam comes in the form of mailing lists. When you sign up to various websites such as comparison sites, you will usually have to add your email address. These companies often have sharing clauses that allow them to share your details with other companies "legitimately". You can normally tell these apart as they will have an unsubscribe option somewhere in the email.

Combatting spam can be very difficult. Spammers often use networks of computers in foreign countries to avoid local anti-spam laws, or even make use of hijacked networks of computers that have been infected with a virus or trojan. These hijacked computers are known as a "Bot-Net". They are a network of hundreds or even thousands of computers that are under the control of a hacker. A hacker who controls such a "Bot-Net" will rent time on these computers to the spammers who then use them to send out millions of spam messages. 

How you get spam
----------------

The first way a spammer gathers their list of addresses is to scan websites. They have built automated tools that scan through web pages with the sole purpose of gathering email addresses. The email addresses they gather are added to a database, which may then be sold on to other spammers.

These automated tools are able to search through search engines such as Google to find new sites - in the same way as you would. Their e-mail address collecting tools then read through every page on the website making a list of any address they find. This includes any public forums you may of posted a message to, any online notice you may have posted or even on your own personal home page.

Another way a spammer gets addresses is through some websites that ask you to register before giving access to certain aspects or to enable you to order a product. Some of these websites may not be as careful with your e-mail address as you would like, some may sell on their mailing list to other parties or they are simply hacked into and your details are stolen.

Another method that results in spam being sent is getting a virus on a persons local computer. The virus will read all the contacts in your address book and begin to start sending spam to them. This is something to be watchful of. If someone you know and trust sends you a suspicious file, don't open it thinking it will defiantly be safe they may have become a victim of spammers.

The final method is that the spammers will simply guess your e-mail address. Once the spammers know your domain name (The last part of your e-mail address, e.g. @schoolemail.co.uk) they will just send off thousands of emails with random names.  So they would e-mail jonh.smith@schoolemail.co.uk, jane.smith@schoolemail.co.uk and so on.

Because they are able to send out millions of spam messages each day, it doesn't matter to them that 90% of these addresses do not get delivered, the 10% that do is more than enough. 

-----------

Dealing with spam
-----------------

If you are receiving spam as an individual, we recommend blocking the sender using OWA or Outlook. To do this click the relevant guide below:

- `Block an address via OWA`_
- `Block an address via Outlook`_

If multiple users in your organisation are receiving the same spam emails, we recommend sending us the headers of those emails. Click the relevant link below to see the guide on getting the headers of your emails.

- `Get headers via OWA`_
- `Get headers via Outlook`_

If you are receiving legitimate emails from known senders and they are being marked as [\*****SPAM*****\], you can add that sender to your organisation's whitelist using the SchoolEmail Portal. For this see our `Portal external whitelist guide`_.

Password security
-----------------

The protection of our users' accounts and data is our top priority. We use the best encryption and certification methods available to us to ensure your accounts are safe and secure.  That being said, the data is only as secure as the password protecting it.

Some passwords can be easily guessed and over the years passwords have needed to become more complex than ever due to more complex programs (viruses/Malware) built by criminals hacking into accounts with brute force.

These programs can also vary on how often they attempt a login and what passwords they try.  Often, they are programmed to try various combinations of the email address and the password of 'password'.
See the recommended Do's and Dont's shown below, when setting passwords.


Don't use the following passwords with any type of combination such as PaSSWoRD, p4ssw0rd etc:

- Password
- Email
- Your Name (This information can be taken from sites that have sign-up forms)
- Admin/Administrator

Don't use the same details in your password as those that make up your email address
Don't use any everyday short word such as hello, open, school, table etc 


Do use as many characters as possible. Recommended minimum is 6.
Do use a combination of uppercase and lowercase characters.
Do use numbers and symbols in the password whenever possible.

----------

Connect via iPhone
------------------

.. Note:: Connecting to a SchoolEmail account via mobile devices requires a premium account.  If you are unsure whether your account is premium or standard, `contact us`_ or your organisations email administrator.

To add a SchoolEmail account to an iPhone, go to the phones 'settings'.

.. image:: ../source/images/iphone1.*
   :scale: 50%

From here click 'Mail, Contacts, Calendars' and then click 'Add Account'.

.. image:: ../source/images/iphone2.*
   :scale: 50%

Select the option 'Exchange'.

.. image:: ../source/images/iphone4.*
   :scale: 50%

Fill in your SchoolEmail account email address and password, feel free to give it any description and click next.

.. image:: ../source/images/iphone3.*
   :scale: 50%

Your phone will connect to the account and start retrieving any mail items the account contains.

Connect via Windows Phone
-------------------------

.. Note:: Connecting to a SchoolEmail account via mobile devices requires a premium account.  If you are unsure whether your account is premium or standard, `contact us`_ or your organisations email administrator.

To add a SchoolEmail account to a Windows Phone, go to the phones 'settings'.

.. image:: ../source/images/wphone1.*
   :scale: 50%

From here click 'email + accounts' and then click 'Add Account'.

.. image:: ../source/images/wphone2.*
   :scale: 50%

Select the option 'Exchange'.

.. image:: ../source/images/wphone3.*
   :scale: 50%

Fill in your SchoolEmail account email address and password and click 'sign in'.

.. image:: ../source/images/wphone4.*
   :scale: 50%

Your phone will connect to the account and pull down any data the account contains.